import React from 'react';
import {Route, Routes} from 'react-router-dom';
import './App.css';
import MainHeader from "./components/MainHeader";
import Welcome from "./pages/Welcome";
import Team from "./pages/Team";
import Gallery from "./pages/Gallery";
import Weather from "./pages/Weather";
import Navigation from "./pages/Navigation";

const App: React.FC = () => {
  return (
    <div>
      <MainHeader/>
      <main>
        <Routes>
          <Route path='/welcome' element={<Welcome/>}/>
        </Routes>
        <Routes>
          <Route path='/team' element={<Team/>}/>
        </Routes>
        <Routes>
          <Route path='/gallery' element={<Gallery/>}/>
        </Routes>
        <Routes>
          <Route path='/weather' element={<Weather/>}/>
        </Routes>
        <Routes>
          <Route path='/navigation' element={<Navigation/>}/>
        </Routes>
      </main>
    </div>
  );
};

export default App;
