import React from 'react';
import {NavLink} from 'react-router-dom';

import classes from "./index.module.css";

const MainHeader: React.FC<{}> = () => {
  return (
    <header className={classes.header}>
      <ul>
        <li>
          <NavLink to='/welcome'>
            Willkommen
          </NavLink>
        </li>
        <li>
          <NavLink to='/team'>
            Team
          </NavLink>
        </li>
        <li>
          <NavLink to='/gallery'>
            Gallerie
          </NavLink>
        </li>
        <li>
          <NavLink to='/weather'>
            Wetter
          </NavLink>
        </li>
        <li>
          <NavLink to='/navigation'>
            Navigation
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default MainHeader;
